﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI_Sample__App.Models;

namespace UI_Sample__App.Controllers
{
    public class HomeController : Controller
    {

        private BLL.Cars cars = new BLL.Cars();

        public ActionResult Index()
        {
            

            return View();
        }



        public JsonResult Get()
        {

            var model = cars.Get();
           
            return Json(new { result = model}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Add()
        {

            var model = new CarViewModel() ;

            return PartialView(model);
        }

        [HttpPost]
        public JsonResult Add(CarViewModel model)
        {

            cars.Add(model);

            return Json(new { result = "success" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Edit(int Id)
        {
            var model = cars.Get(Id);


            return PartialView(model);
        }

        [HttpPost]
        public JsonResult Edit(CarViewModel model)
        {

            cars.Edit(model);

            return Json(new { result = "success" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            return PartialView(Id);
        }

        [HttpPost]
        public JsonResult DeleteCar(int Id)
        {

            cars.Delete(Id);

            return Json(new { result = "success" }, JsonRequestBehavior.AllowGet);
        }

    }
}