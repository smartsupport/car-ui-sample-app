﻿
var app = angular.module('myApp', []);

var x = 0, y = 0;
app.controller('Ctrl', ['$document', '$scope', '$http', '$location', '$window', '$compile', function ($document, $scope, $http, $location, $window, $compile) {
    // Get Table Data from server
    $scope.review = function () {
        var url ="http://" + $location.absUrl().split('/')[2] + '/Home/Get';
      
        $http({
            method: 'GET',
            url: url
        }).then(function successCallback(response) {
            $scope.Items = response.data.result;
        }, function errorCallback(response) {
            alert("error");
        });
    }
    $scope.review();

    // Open Dialog
    $scope.ShowDialog = function (action, id) {
        $scope.isDisplayed = true;
        $scope.form = {};
        if (action != undefined) {
            $http({
                method: "GET",
                url: action,
                params: { 'id': id },
                headers: { 'Content-Type': 'application/json' }
            })
                .success(function (data) {
                    if (data.error) {
                        // Showing errors.
                        alert("error");
                    } else {
                        var e = angular.element(data);
                        $compile(e.contents())($scope);

                        angular.element(document.body).append(e);
                        setTimeout(function () {


                            //$scope.form.$setUntouched();
                            $scope.error = {};
                            $scope.$apply();
                            x = ($window.innerWidth / 2) - 200;
                            y = 80;
                            angular.element(document.querySelector(".IbDailog")).css({ "left": ($window.innerWidth / 2) - 200 + 'px', "top": "80px" });
                        }, 5);

                    }

                });
        }

    }

    //Dialog Remove
    $scope.removeDialog = function () {
        var dialogY = angular.element(document.getElementsByClassName("IbDailog"));
        dialogY.remove();
        $scope.isDisplayed = false;
    }


    // Dialog Form Submit Action
    $scope.submitForm = function (action, method) {
   
        $http({
            method: method,
            url: action,
            data: $scope.form,
            headers: { 'Content-Type': 'application/json' }
        })
            .success(function (data) {
                if (data.error) {
                    // Showing errors.
                    $scope.error = data.error;
                } else {
                    $scope.removeDialog();
                    $scope.review();
                }
            });
    }



}]);

app.directive('toggleClass', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                if (!element.parent().hasClass("open")) {
                    angular.element(document.querySelector(".open")).removeClass("open");
                    element.parent().toggleClass(attrs.toggleClass);
                } else {
                    angular.element(document.querySelector(".open")).removeClass("open");
                   
                }
               
            });
        }
    };
});

app.directive('draggable', function ($document, $window) {
    return function (scope, element, attr) {
        var startX = 0,
            startY = 0;
        x = ($window.innerWidth / 2) - 200;
        y = 80;
        var container = null;

        element.css({
            position: 'relative',
            cursor: 'move'
        });

        element.on('mousedown', function (event) {
            // Prevent default dragging of selected content
            event.preventDefault();
            startX = event.screenX - x;
            startY = event.screenY - y;
            $document.on('mousemove', mousemove);
            $document.on('mouseup', mouseup);
            container = attr.$$element.parent().parent();
        });

        function mousemove(event) {
            y = event.screenY - startY;
            x = event.screenX - startX;

            container.css({
                top: y + 'px',
                left: x + 'px'
            });
        }

        function mouseup() {
            $document.unbind('mousemove', mousemove);
            $document.unbind('mouseup', mouseup);
        }
    }
});


