﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UI_Sample__App.Models;

namespace UI_Sample__App.BLL
{
    public class Cars
    {
        
        //public Cars()
        //{
        //    db = new List<StaticDataBase>();
        //}

        private static int MaxId { get; set; }
      

        public IEnumerable<CarViewModel> Get()
        {
            var model = StaticDataBase.cars.Select(x =>  new CarViewModel {Id = x.Id, Manufacture = { Value = x.Manufacture }, Make = {  Value = x.Make} , Year = { Value = x.year }, Colour = {  Value =x.Colour}, Passenger = {  Value = x.Passenger} });
            return  model;
        }
        
        public CarViewModel Get(int Id)
        {
            var model = StaticDataBase.cars.Where(x => x.Id == Id).Select(x => new CarViewModel { Id = x.Id, Manufacture = { Value = x.Manufacture }, Make = { Value = x.Make }, Year = { Value = x.year }, Colour = { Value = x.Colour }, Passenger = { Value = x.Passenger } }).Single();
            return model;
        }

        public Boolean Add (CarViewModel model)
        {
            MaxId ++ ;
            StaticDataBase.cars.Add(new CarModel { Id = MaxId, Manufacture = model.Manufacture.Value, Make = model.Make.Value, year = model.Year.Value, Colour = model.Colour.Value , Passenger = model.Passenger.Value });
            return true;
        }

        public Boolean Edit (CarViewModel model)
        {
            var item = StaticDataBase.cars.Where(x => x.Id == model.Id).FirstOrDefault();
            item.Manufacture = model.Manufacture.Value;
            item.Make = model.Make.Value;
            item.year = model.Year.Value;
            item.Colour = model.Colour.Value;
            item.Passenger = model.Passenger.Value;

            return true;
        }


        public Boolean Delete(int Id)
        {
            var model = StaticDataBase.cars.Where(x => x.Id == Id).FirstOrDefault();
            StaticDataBase.cars.Remove(model);


            return true;
        }

    }
}