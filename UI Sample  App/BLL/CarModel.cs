﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI_Sample__App.BLL
{
    public class CarModel
    {
        public int Id { get; set; }
        public int Manufacture { get; set; }
        public int Make { get; set; }
        public int year { get; set; }
        public int Colour { get; set; }
        public int Passenger { get; set; }
    }
}