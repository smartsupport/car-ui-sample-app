﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI_Sample__App.Models;

namespace UI_Sample__App.Models
{
    public class CarViewModel
    {

        private List<int> _year =new List<int>(); 
        public CarViewModel()
        {
       

            Manufacture = new DropDownListModel<ManufactureList>();
            Make = new DropDownListModel<MakeList>();

            for (var i = 2017; i >= 2000; i--)
            {
                _year.Add(i);
            }
            Year = new YearVal();

            Colour = new DropDownListModel<ColourList>();
            Passenger = new DropDownListModel<PassengerList>();
        }

        public int Id { get;  set; }

        [UIHint("DropDownList")]
        public ManufactureList ManufactureList { get; set; }
        public DropDownListModel<ManufactureList> Manufacture { get; set; }

        [UIHint("DropDownList")]
        public MakeList MakeList { get; set; }
        public DropDownListModel<MakeList> Make { get; set; }

        [UIHint("IntDropDownList")]
        public List<int> YearList { get { return _year; } set { _year = value; } }
        public YearVal Year { get; set; }

        [UIHint("DropDownList")]
        public ColourList ColourList { get; set; }
        public DropDownListModel<ColourList> Colour { get; set; }

        [UIHint("DropDownList")]
        public PassengerList PassengerList { get; set; }
        public DropDownListModel<PassengerList> Passenger { get; set; }

    }



}

public enum ManufactureList
{
    Canada = 0,
    USA = 1,
    China = 2,
    Germany = 3,
    Korea = 4

}

public enum MakeList
{
    BMW = 0,
    Toyota = 1,
    Dodge = 2,
    Ford = 3,
    Nissan = 4

}

public enum ColourList
{
    Red = 0,
    Blue = 1,
    Gray = 2,
    Black = 3,
    white = 4

}

public enum PassengerList
{
    Five = 0,
    Four = 1,
    Eight = 2,
    Two = 3
}


