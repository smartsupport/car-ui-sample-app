﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UI_Sample__App.Models
{
    public class YearVal
    {
        private List<int> _List = new List<int>();
        private string _Text;
        private int _Value;


        public YearVal()
        {
            for (var i = 2017; i >= 2000; i--)
            {
                _List.Add(i);
            }
            this.Value = 2017;
        }


        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public int Value
        {
            get { return _Value; }
            set { ChangeVal(value); }
        }

        public void ChangeVal(int val)
        {
            _Value = val;
            _Text = _List.Where(x => x == _Value).Single().ToString();
        }

    }
}