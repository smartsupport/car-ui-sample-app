﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UI_Sample__App.Models
{
    public class DropDownListModel<T> where T:struct
    {
        private List<SelectListItem> _List;
        private string _Text;
        private int _Value ;


        public DropDownListModel()
        {
            _List = Enum.GetValues(typeof(T)).Cast<T>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = Convert.ToInt32(v).ToString()
            }).ToList();
            this.Value = 0;
        }


        public string Text {
            get { return _Text; }
            set { _Text = value; }
        }

        public int Value
        {
            get { return _Value; } 
            set { ChangeVal(value); }
        }

        public void ChangeVal(int val)
        {
            _Value = val;
            _Text = _List.Where(x => x.Value == _Value.ToString()).Single().Text;
        }
    }
}